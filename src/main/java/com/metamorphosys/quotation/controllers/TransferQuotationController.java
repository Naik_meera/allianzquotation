package com.metamorphosys.quotation.controllers;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.dataobjects.transaction.QuotationDO;
import com.metamorphosys.insureconnect.jpa.transaction.QuotationRepository;
import com.metamorphosys.quotation.utility.SerializationUtility;


@RestController
@RequestMapping("/api/transferQuotation")
public class TransferQuotationController {

	@Autowired
	QuotationRepository quotationRepository;
	
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity downloadQuotation(@RequestBody @Valid String json) {
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		HashMap<String, String> map = (HashMap<String, String>) SerializationUtility.getInstance().fromJson(json, HashMap.class);
		Date d=null;
		if(map.get("lastSynDt")!=null){
			 SimpleDateFormat sdf1 = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss");
			 java.util.Date date = null;
			try {
				date = sdf1.parse(map.get("lastSynDt"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			 d = new java.sql.Date(date.getTime());
		}
		
		List<QuotationDO> quotationDOList =quotationRepository.findByBaseAgentCdAndQuotationDtIsNullOrQuotationDtAfter((String)map.get("baseAgentId"),d);
		
		String jsonResponse=SerializationUtility.getInstance().toJson(quotationDOList);
		return new ResponseEntity(jsonResponse,httpHeaders,httpStatus);
	}
}
