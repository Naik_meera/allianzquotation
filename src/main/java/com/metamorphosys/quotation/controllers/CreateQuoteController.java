package com.metamorphosys.quotation.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.dataobjects.transaction.QuotationDO;
import com.metamorphosys.quotation.Executor.QuoteExecutor;
import com.metamorphosys.quotation.interfaceobjects.CreateQuotationIO;

@RestController
@RequestMapping("/api/createQuote")
public class CreateQuoteController {

private static final Logger LOGGER =  LoggerFactory.getLogger(CreateQuoteController.class);
	
	private final QuoteExecutor quoteExecutor;
	
	@Autowired
	public CreateQuoteController (QuoteExecutor quoteExecutor) {
        this.quoteExecutor = quoteExecutor;
    }
	
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity init(){
		LOGGER.info("init CreateQuoteController");
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		//httpHeaders.setAccessControlAllowMethods();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		CreateQuotationIO io = new CreateQuotationIO();
		QuotationDO quotationDO= new QuotationDO();
		io.setQuotationDO(quotationDO);
		io=quoteExecutor.init(io);
		return new ResponseEntity(io, httpHeaders, httpStatus);		
	}
	
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity save(@RequestBody @Valid CreateQuotationIO io){
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("save CreateQuoteController");
		io=quoteExecutor.execute(io);
		return new ResponseEntity(io, httpHeaders, httpStatus);			
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	ResponseEntity fetch(@PathVariable("id") Long id) {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("save PolicyController");
		
		QuotationDO quotationDO = new QuotationDO();
		quotationDO.setId(id);
		CreateQuotationIO io = new CreateQuotationIO();
		io.setQuotationDO(quotationDO);
		io=quoteExecutor.load(io);
		
		return new ResponseEntity(io, httpHeaders, httpStatus);	
		
	}
}
