package com.metamorphosys.quotation.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.transaction.QuotationDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceTaskDO;
import com.metamorphosys.insureconnect.utilities.SequenceGeneratorUtil;
import com.metamorphosys.quotation.interfaceobjects.CreateQuotationIO;
import com.metamorphosys.quotation.utility.GuidGeneratorUtility;

@Component
public class QuoteExecutor extends AbstractExecutor{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(QuoteExecutor.class);
	
	@Autowired
	SequenceGeneratorUtil sequenceGeneratorUtil;
	
	/*@Autowired
	public QuoteExecutor(SequenceGeneratorUtil sequenceGeneratorUtil) {
		this.sequenceGeneratorUtil =sequenceGeneratorUtil;
	}*/
	
	@Override
	public CreateQuotationIO init(CreateQuotationIO io) {
		if(io.getServiceDO()==null){
			io.setServiceDO(getService());
		}
		super.init(io);
		return io;
	}

	@Override
	public CreateQuotationIO execute(CreateQuotationIO io) {
		QuotationDO quotationDO = (QuotationDO) io.getQuotationDO();
		generateQuotationRefNum(quotationDO);
		super.execute(io);
		return io;
	}

	private void generateQuotationRefNum(QuotationDO quotationDO) {
		if (quotationDO.getQuotationRefNum() == null) {
			quotationDO.setQuotationRefNum(sequenceGeneratorUtil.generateNextSeq("quotationRefNum"));
		}
	}

	@Override
	public ServiceDO getService() {
		ServiceDO serviceDO = new ServiceDO();
		serviceDO.setService("CREATEQUOTE");
		serviceDO.setServiceType("WORKFLOW");
		serviceDO.setGuid(GuidGeneratorUtility.getInstance().generateGuid());
		ServiceTaskDO task = new ServiceTaskDO();
		task.setServiceTask("init");
		serviceDO.getServiceTaskList().add(task);
		return serviceDO;
	}

}
