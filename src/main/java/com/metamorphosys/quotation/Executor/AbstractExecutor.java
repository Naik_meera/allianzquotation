package com.metamorphosys.quotation.Executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.google.gson.internal.LinkedTreeMap;
import com.metamorphosys.insureconnect.controllers.WebExecuteController;
import com.metamorphosys.insureconnect.controllers.WebGetController;
import com.metamorphosys.insureconnect.dataobjects.transaction.QuotationDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.insureconnect.jpa.transaction.QuotationRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.quotation.dataobjects.common.ResultDO;
import com.metamorphosys.quotation.interfaceobjects.BaseIO;
import com.metamorphosys.quotation.interfaceobjects.CreateQuotationIO;
import com.metamorphosys.quotation.utility.SerializationUtility;

public abstract class AbstractExecutor {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractExecutor.class);
	
	@Autowired
	QuotationRepository quotationRepository;
	
	@Autowired
	WebExecuteController webExecuteController;
	
	@Autowired
	WebGetController webGetController;
	
	public CreateQuotationIO init(CreateQuotationIO io) {
		io = callRuleEngine(io,"EXECUTE");
		return io;
		
	}

	public CreateQuotationIO execute(CreateQuotationIO io) {
		io = callRuleEngine(io,"EXECUTE");
		//if(io.getServiceDocumentDOs().size()>0){
		//serviceDocumentRepository.save(io.getServiceDocumentDOs());
	//}
	
	//if(io.getServiceQuestionReponseDOs().size()>0){
		//serviceQuestionResponseRepository.save(io.getServiceQuestionReponseDOs());
	//}
		quotationRepository.save((QuotationDO)io.getQuotationDO());
		return io;
	}
	
	public CreateQuotationIO load(CreateQuotationIO io) {
		
		QuotationDO quotationDO = quotationRepository.findById( io.getQuotationDO().getId());
		io.setQuotationDO(quotationDO);
		return io;
	}

	private CreateQuotationIO callRuleEngine(CreateQuotationIO io,String method) {

		String tojson = covertTojson(io);
		LOGGER.info(tojson);
		ResponseEntity<String> jsonObject=null;
		if(InsureConnectConstants.Method.EXECUTE.equals(method)){
			jsonObject = webExecuteController.executeJson(tojson);
		}else if(InsureConnectConstants.Method.GET.equals(method)){
			jsonObject = webGetController.execute(tojson);
		}
		/*RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(tojson.toString(), httpHeaders);
		ResponseEntity<String> jsonObject = restTemplate.exchange(url,
				HttpMethod.POST, entity, String.class);
*/		io = convertjsonToObject(jsonObject.getBody(), io);

		return io;
	}

	private CreateQuotationIO convertjsonToObject(String json, CreateQuotationIO io) {

		String data = null;
		LinkedTreeMap map = SerializationUtility.getInstance().fromJson(json);

		if (null != map && map.containsKey("data")) {
			data = map.get("data").toString();
		}
		
		ArrayList<LinkedTreeMap> responseDataList = (ArrayList<LinkedTreeMap>) SerializationUtility.getInstance().fromJson(data, ArrayList.class);
		if (null != responseDataList) {
			for (LinkedTreeMap responseDataIO : responseDataList) {
				if (InsureConnectConstants.DataObjects.QUOTATIONDO.equals(responseDataIO.get("objectName").toString())) {

					QuotationDO baseDO = (QuotationDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), QuotationDO.class);
					io.setQuotationDO(baseDO);
				}
				if (InsureConnectConstants.DataObjects.SERVICEDO.equals(responseDataIO.get("objectName").toString())) {
					ServiceDO baseDO = (ServiceDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), ServiceDO.class);

					io.setServiceDO(baseDO);
				}
				if (InsureConnectConstants.DataObjects.RESULTDO.equals(responseDataIO.get("objectName").toString())) {
					ResultDO baseDO = (ResultDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), ResultDO.class);

					io.setResultDO(baseDO);
				}

			}
		}
		return io;
	}

	public String covertTojson(CreateQuotationIO io) {
		List<BaseIO> baseIOs = new ArrayList<BaseIO>();

		BaseIO baseIO = new BaseIO();
		baseIO.setObjectName(io.getQuotationDO().getObjectName());
		baseIO.setObject(SerializationUtility.getInstance().toJson(io.getQuotationDO()));
		baseIOs.add(baseIO);

		BaseIO baseIO1 = new BaseIO();
		baseIO1.setObjectName(io.getServiceDO().getObjectName());
		baseIO1.setObject(SerializationUtility.getInstance().toJson(io.getServiceDO()));
		baseIOs.add(baseIO1);

		String serialize = SerializationUtility.getInstance().toJson(baseIOs);
		HashMap<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("data", serialize);
		String finalString = SerializationUtility.getInstance().toJson(dataMap);
		

		return finalString;

	}

	public abstract ServiceDO getService();

}
