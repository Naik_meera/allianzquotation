package com.metamorphosys.quotation.dataobjects;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(entityManagerFactoryRef = "quotationEntityManagerFactory",
		transactionManagerRef = "quotationTransactionManager", basePackages={"com.metamorphosys.quotation.repository"})
public class QuotationConfig {

	@Bean
	PlatformTransactionManager quotationTransactionManager() {
		return new JpaTransactionManager(quotationEntityManagerFactory().getObject());
	}

	@Bean
	LocalContainerEntityManagerFactoryBean quotationEntityManagerFactory() {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(false);
		vendorAdapter.setDatabase(Database.ORACLE);
		vendorAdapter.setShowSql(true);
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		factoryBean.setDataSource(quotationDataSource());
		factoryBean.setJpaVendorAdapter(vendorAdapter);
		factoryBean.setPackagesToScan(this.getClass().getPackage().getName());

		return factoryBean;
	}

	
	@Bean
	@ConfigurationProperties(prefix="primary.spring.datasource")
	public DataSource quotationDataSource() {
	    return DataSourceBuilder.create().build();
	}

}

