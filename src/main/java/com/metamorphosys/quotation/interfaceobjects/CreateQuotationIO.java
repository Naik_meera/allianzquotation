package com.metamorphosys.quotation.interfaceobjects;

import java.util.List;

import com.metamorphosys.insureconnect.dataobjects.transaction.QuotationDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.quotation.dataobjects.common.ResultDO;

public class CreateQuotationIO {

	public  QuotationDO quotationDO;
	
	public List<QuotationDO> quotationDOList;

	public ServiceDO serviceDO;
	
	public ResultDO resultDO;

		public QuotationDO getQuotationDO() {
		return quotationDO;
	}

	public void setQuotationDO(QuotationDO quotationDO) {
		this.quotationDO = quotationDO;
	}

	public List<QuotationDO> getQuotationDOList() {
		return quotationDOList;
	}

	public void setQuotationDOList(List<QuotationDO> quotationDOList) {
		this.quotationDOList = quotationDOList;
	}

	public ResultDO getResultDO() {
		return resultDO;
	}

	public void setResultDO(ResultDO resultDO) {
		this.resultDO = resultDO;
	}

		public ServiceDO getServiceDO() {
		return serviceDO;
	}

	public void setServiceDO(ServiceDO serviceDO) {
		this.serviceDO = serviceDO;
	}
	
}
