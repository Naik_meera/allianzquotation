package com.metamorphosys.quotation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.metamorphosys.quotation.QuotationApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = QuotationApplication.class)
@WebAppConfiguration
public class QuotationApplicationTests {

	@Test
	public void contextLoads() {
	}

}
